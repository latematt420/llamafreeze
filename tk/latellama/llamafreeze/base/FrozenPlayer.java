package tk.latellama.llamafreeze.base;

import org.bukkit.World;
import org.bukkit.entity.Player;

public class FrozenPlayer {
    private final String name;
    private final double x, y, z;
    private final World world;
    private final float yaw, pitch;

    public FrozenPlayer(final Player player) {
        this(player.getName(), player.getWorld(), player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), player.getEyeLocation().getYaw(), player.getEyeLocation().getPitch());
    }

    public FrozenPlayer(final String name, final World world, final double x, final double y, final double z, final float yaw, final float pitch) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.z = z;
        this.world = world;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public final String getName() {
        return name;
    }

    public final double getX() {
        return x;
    }

    public final double getY() {
        return y;
    }

    public final double getZ() {
        return z;
    }

    public final World getWorld() {
        return world;
    }

    public final float getYaw() {
        return yaw;
    }

    public final float getPitch() {
        return pitch;
    }
}
