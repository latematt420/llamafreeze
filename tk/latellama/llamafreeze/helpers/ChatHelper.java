package tk.latellama.llamafreeze.helpers;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class ChatHelper {
    public final void sendMessage(final CommandSender sender, final String message) {
        sender.sendMessage(ChatColor.DARK_AQUA + "[LlamaFreeze]: " + ChatColor.WHITE + message);
    }
}
