package tk.latellama.llamafreeze.helpers;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.latellama.llamafreeze.base.FrozenPlayer;

import java.util.ArrayList;
import java.util.List;

public class FreezeHelper {
    private final List<FrozenPlayer> frozenPlayers = new ArrayList<FrozenPlayer>();

    public final List<FrozenPlayer> getFrozenPlayers() {
        return frozenPlayers;
    }

    public final void addPlayer(final Player player) {
        frozenPlayers.add(new FrozenPlayer(player));
    }

    public final void removePlayer(final Player player) {
        for (int index = 0; index < frozenPlayers.size(); index++) {
            final FrozenPlayer frozenPlayer = frozenPlayers.get(index);
            if (frozenPlayer.getName().equals(player.getName())) {
                frozenPlayers.remove(index);
            }
        }
    }

    public final boolean isFrozen(final CommandSender sender) {
        for (final FrozenPlayer frozenPlayer : frozenPlayers) {
            if (sender.getName().equals(frozenPlayer.getName())) {
                return true;
            }
        }
        return false;
    }

    public final FrozenPlayer getFrozenPlayer(final Player player) {
        for (final FrozenPlayer frozenPlayer : frozenPlayers) {
            if (frozenPlayer.getName().equals(player.getName())) {
                return frozenPlayer;
            }
        }
        return null;
    }
}
