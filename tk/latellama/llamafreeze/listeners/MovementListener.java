package tk.latellama.llamafreeze.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import tk.latellama.llamafreeze.Main;
import tk.latellama.llamafreeze.base.FrozenPlayer;

public class MovementListener implements Listener {
    private final Main plugin;
    public MovementListener(final Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public final void onMovement(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        if (plugin.getFreezeHelper().isFrozen(player)) {
            final FrozenPlayer frozenPlayer = plugin.getFreezeHelper().getFrozenPlayer(player);
            final Location previousLocation = new Location(frozenPlayer.getWorld(), frozenPlayer.getX(), frozenPlayer.getY(), frozenPlayer.getZ(), frozenPlayer.getYaw(), frozenPlayer.getPitch());
            player.teleport(previousLocation);
        }
    }
}
