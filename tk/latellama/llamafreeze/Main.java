package tk.latellama.llamafreeze;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import tk.latellama.llamafreeze.helpers.ChatHelper;
import tk.latellama.llamafreeze.helpers.FreezeHelper;
import tk.latellama.llamafreeze.listeners.MovementListener;

import java.util.logging.Logger;

public class Main extends JavaPlugin {
    private Logger logger;
    private ChatHelper chatHelper;
    private FreezeHelper freezeHelper;

    @Override
    public void onEnable() {
        logger = getLogger();
        chatHelper = new ChatHelper();
        freezeHelper = new FreezeHelper();
        MovementListener movementListener = new MovementListener(this);
        getServer().getPluginManager().registerEvents(movementListener, this);
        logger.info("LlamaFreeze enabled.");
    }

    @Override
    public void onDisable() {
        logger.info("LlamaFreeze disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("freeze")) {
            if (args.length == 0) {
                chatHelper.sendMessage(sender, "Usage: /freeze <player name>");
                return true;
            }

            if (!sender.hasPermission("llamafreeze.freeze")) {
                chatHelper.sendMessage(sender, "You don't have permission!");
                return true;
            }

            if (freezeHelper.isFrozen(sender) && args[0].equalsIgnoreCase(sender.getName())) {
                chatHelper.sendMessage(sender, "You cannot unfreeze yourself!");
                return true;
            }

            for (final Player player : Bukkit.getOnlinePlayers()) {
                if (args[0].equalsIgnoreCase(player.getName())) {
                    if (!freezeHelper.isFrozen(player)) {
                        freezeHelper.addPlayer(player);
                        chatHelper.sendMessage(sender, "Player " + player.getName() + " frozen.");
                    } else {
                        freezeHelper.removePlayer(player);
                        chatHelper.sendMessage(sender, "Player " + player.getName() + " unfrozen.");
                    }
                    return true;
                }
            }
            chatHelper.sendMessage(sender, "Can't find " + args[0]);
            return true;
        }
        return false;
    }

    public FreezeHelper getFreezeHelper() {
        return freezeHelper;
    }
}
